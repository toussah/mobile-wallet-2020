from decimal import Decimal
from rest_framework import serializers
from transaction.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True, min_value=Decimal(0.01))
    sender_username = serializers.CharField(source='sender.user.username', required=False)
    recipient_username = serializers.CharField(source='recipient.user.username', required=False)

    class Meta:
        model = Transaction
        fields = ('sender', 'recipient', 'amount', 'reason', 'status', 'failed_reason', 'send_time', 'last_updated',
                  'sender_username', 'recipient_username')
        read_only_fields = ('status', 'failed_reason', 'send_time', 'last_updated', 'sender_username', 'recipient_username')
