from django.urls import path
from transaction import views


urlpatterns = [
    path('api/v1/transactions', views.TransactionList.as_view(), name="transactions"),
    path('api/v1/transfer/<str:username>', views.TransferAPI.as_view(), name="transfer"),
]
