from accounts.models import User
from copy import deepcopy
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from transaction.models import Transaction
from transaction.serializers import TransactionSerializer


class TransactionList(APIView):
    """
    List the user's transactions in reversed chronological order.
    A user is part of a transaction if he's either sender or recipient.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user.user
        transactions = Transaction.objects.filter(Q(sender=user) | Q(recipient=user)).order_by("-send_time")
        return Response(TransactionSerializer(transactions, many=True).data)


class TransferAPI(APIView):
    """
    Transfer amount of money to user.
    The username (unique) of the user must be part of the URL parameters.
    The POST payload must contain the amount of the transfer, an optional `reason` can be given.
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request, username, format=None):
        sender = request.user.user
        recipient = get_object_or_404(User, user__username=username)
        request_data = deepcopy(request.data)
        request_data.update({"sender": sender.pk, "recipient": recipient.pk})
        transaction = TransactionSerializer(data=request_data)
        if transaction.is_valid():
            transaction_obj = transaction.save()
        else:
            return Response({"errors": transaction.errors}, status=status.HTTP_400_BAD_REQUEST)
        success = transaction_obj.transfer()
        if not success:
            return Response({"errors": transaction_obj.failed_reason}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"success": True}, status=status.HTTP_200_OK)
