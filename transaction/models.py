from accounts.models import User
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models, transaction
from django.utils import timezone


class Transaction(models.Model):
    """
    Tracks all user transactions.
    """
    PENDING = 'pending'
    COMPLETED = 'completed'
    FAILED = 'failed'
    status_choices = [
        (PENDING, 'Pending'),
        (COMPLETED, 'Completed'),
        (FAILED, 'Failed')
    ]

    sender = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="transactions_sent")
    recipient = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name="transactions_received")
    amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    reason = models.CharField(max_length=255, blank=True, null=False)
    status = models.CharField(max_length=32, default=PENDING, null=False, choices=status_choices)
    failed_reason = models.CharField(max_length=255, blank=False, null=True)
    send_time = models.DateTimeField(default=timezone.now)
    last_updated = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [
            models.Index(fields=('send_time',))
        ]

    def transfer(self):
        """
        Transfers money from sender to receiver.
        The sender must have enough funds for the transaction.
        Concurrency is dealt with through an atomic transaction.
        """
        with transaction.atomic():
            if float(self.sender.fund) < self.amount:
                # Check before to avoid doing the transaction if the sender doesn't have the necessary funds.
                self.status = Transaction.FAILED
                self.failed_reason = "Insufficient fund."
            else:
                User.objects.filter(pk=self.sender.pk).update(fund=models.F('fund') - self.amount)
                User.objects.filter(pk=self.recipient.pk).update(fund=models.F('fund') + self.amount)
                if User.objects.filter(pk=self.sender.pk).values_list('fund', flat=True).first() < 0:
                    # Check after as well in case of concurrency and rollback transaction if funds too low.
                    transaction.set_rollback(True)
                    self.status = Transaction.FAILED
                    self.failed_reason = "Insufficient fund."
                else:
                    self.status = Transaction.COMPLETED

        self.save()
        return self.status == Transaction.COMPLETED
