from accounts.models import BaseUser, User
from decimal import Decimal
from django.test import TestCase
from transaction.models import Transaction


class TestTransactions(TestCase):
    """
    Tests methods belonging to the Transaction class.
    """

    def setUp(self):
        """
        Creates a sender and a recipient to make transaction creation quicker.
        """
        user_sender = BaseUser.objects.create(username="sender")
        self.sender = User.objects.get(user=user_sender)
        self.sender.fund = "2750.15"
        self.sender.save()

        user_recipient = BaseUser.objects.create(username="recipient")
        self.recipient = User.objects.get(user=user_recipient)
        self.recipient.fund = "1500"
        self.recipient.save()

    def tearDown(self):
        """
        Deletes Transactions, BaseUsers, Users and Tokens created in setUp.
        """
        BaseUser.objects.all().delete()  # Related objects are deleted as a side-effect of DELETE ON CASCADE

    def test_transfer(self):
        """
        Tests Transaction's transfer method
        """
        # Regular transfer.
        transaction = Transaction.objects.create(sender=self.sender, recipient=self.recipient, amount=100,
                                                 reason="Jennifer's present")
        self.assertTrue(transaction.transfer())
        self.sender.refresh_from_db()
        self.recipient.refresh_from_db()
        # Check that money was transferred
        self.assertEqual(self.sender.fund, Decimal("2650.15"))
        self.assertEqual(self.recipient.fund, Decimal("1600.00"))

        # Transfer with lack of fund
        transaction = Transaction.objects.create(sender=self.sender, recipient=self.recipient, amount=2651,
                                                 reason="For the trip to Bali")
        self.assertFalse(transaction.transfer())
        self.sender.refresh_from_db()
        self.recipient.refresh_from_db()
        # Check that money was not transferred
        self.assertEqual(self.sender.fund, Decimal("2650.15"))
        self.assertEqual(self.recipient.fund, Decimal("1600.00"))
        self.assertEqual(transaction.status, Transaction.FAILED)
        self.assertEqual(transaction.failed_reason, "Insufficient fund.")
