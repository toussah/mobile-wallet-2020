from accounts.models import BaseUser, User
from django.db.models import Q
from django.test import TestCase
from rest_framework.test import force_authenticate, APIRequestFactory
from transaction.models import Transaction
from transaction.serializers import TransactionSerializer
from transaction.views import TransactionList, TransferAPI

# Using the standard RequestFactory API to create an HTTP request
factory = APIRequestFactory()


class TestTransactionViews(TestCase):
    """
    Tests the transaction's views.
    """

    def setUp(self):
        """
        Creates three user to make transaction creation quicker.
        """
        base_user_a = BaseUser.objects.create(username="user_a")
        self.user_a = User.objects.get(user=base_user_a)
        self.user_a.fund = "2750.15"
        self.user_a.save()

        base_user_b = BaseUser.objects.create(username="user_b")
        self.user_b = User.objects.get(user=base_user_b)
        self.user_b.fund = "1950.0"
        self.user_b.save()

        base_user_c = BaseUser.objects.create(username="user_c")
        self.user_c = User.objects.get(user=base_user_c)
        self.user_c.fund = "750.35"
        self.user_c.save()

        self.transfers = []
        self.transactions = [
            (self.user_a, self.user_b, [10, 100, 3000]),
            (self.user_b, self.user_c, [100, 300, 1000]),
            (self.user_c, self.user_a, [50, 700]),
            (self.user_a, self.user_c, [1000, 30]),
        ]

    def tearDown(self):
        """
        Deletes Transactions, BaseUsers, Users and Tokens created in setUpClass.
        """
        BaseUser.objects.all().delete()  # Related objects are deleted as a side-effect of DELETE ON CASCADE

    def test_transaction_list(self):
        """
        Tests for each user created in setUp that we can use the UserFund view to retrieve their information.
        """
        # Create some transactions.
        for sender, recipient, amounts in self.transactions:
            for amount in amounts:
                Transaction.objects.create(sender=sender, recipient=recipient, amount=amount)

        view = TransactionList.as_view()
        request = factory.get('api/v1/transactions')
        for user in (self.user_a, self.user_b, self.user_c):
            force_authenticate(request, user=user.user)
            response = view(request)
            serializer = TransactionSerializer(Transaction.objects.filter(Q(sender=user) | Q(recipient=user)).order_by("-send_time"),
                                               many=True)
            self.assertEqual(response.data, serializer.data)

    def test_transfer(self):
        """
        Tests for each user created in setUp that we can use the UserFund view to retrieve their information.
        """
        view = TransferAPI.as_view()
        for sender, recipient, amounts in self.transactions:
            end_point = f'api/v1/transfer/?username={recipient.user.username}'
            for amount in amounts:
                request = factory.post(end_point, {"amount": amount})
                force_authenticate(request, user=sender.user)
                response = view(request, username=recipient.user.username)
                transaction = Transaction.objects.order_by('-send_time').first()
                self.assertIn(transaction.status, (Transaction.FAILED, Transaction.COMPLETED))
                if transaction.status == Transaction.FAILED:
                    self.assertIn("errors", response.data)
                else:
                    self.assertDictEqual({"success": True}, dict(response.data))

    def test_non_authenticated_transfer(self):
        """
        Checks that the user must be authenticated for the transfer API.
        """
        view = TransferAPI.as_view()
        sender, recipient, amounts = self.transactions[0]
        amount = amounts[0]
        request = factory.post(f'api/v1/transfer/?username={recipient.user.username}', {"amount": amount})
        response = view(request, username=recipient.user.username)
        self.assertIn('detail', response.data)
        self.assertIn('Authentication', response.data['detail'])
