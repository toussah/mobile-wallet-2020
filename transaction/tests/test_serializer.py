from accounts.models import BaseUser, User
from copy import deepcopy
from django.test import TestCase
from transaction.serializers import TransactionSerializer
from transaction.models import Transaction


class TestTransactionSerializer(TestCase):
    """
    Tests the TransactionSerializer.
    """
    @classmethod
    def setUpClass(cls):
        """
        Creates two dummy users and a dummy transaction to test the serializer.
        """
        super().setUpClass()
        user_sender = BaseUser.objects.create(username="sender")
        cls.sender = User.objects.get(user=user_sender)
        user_recipient = BaseUser.objects.create(username="recipient")
        cls.recipient = User.objects.get(user=user_recipient)
        cls.transaction_attributes = {
            "sender": cls.sender,
            "recipient": cls.recipient,
            "amount": "70.00",
            "reason": "Mom's birthday"
        }

        # Sample of valid serializer data, will be updated in various tests to check edge cases.
        cls.serializer_data = {
            "sender": cls.sender.pk,
            "recipient": cls.recipient.pk,
            "amount": "15",
            "reason": "Dinner last week!",
        }
        cls.transaction = Transaction.objects.create(**cls.transaction_attributes)
        cls.transaction_serializer = TransactionSerializer(instance=cls.transaction)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes Transactions, BaseUsers, Users and Tokens created in setUpClass.
        """
        super().tearDownClass()
        BaseUser.objects.all().delete()  # Related objects are deleted as a side-effect of DELETE ON CASCADE

    def test_contains_expected_content(self):
        data = self.transaction_serializer.data
        expected_fields = ('sender', 'recipient', 'amount', 'reason', 'status', 'failed_reason', 'send_time', 'last_updated',
                           'sender_username', 'recipient_username')
        self.assertCountEqual(data.keys(), expected_fields,
                              msg=f"Discrepancy in fields found in serializer, expected {expected_fields}, found {list(data.keys())}")

        write_fields = ('amount', 'reason')
        for write_field in write_fields:
            self.assertEqual(data[write_field], self.transaction_attributes[write_field])

        fk_fields = ('sender', 'recipient')
        for fk_field in fk_fields:
            self.assertEqual(data[fk_field], self.transaction_attributes[fk_field].pk)

    def test_amount(self):
        """
        Tests that the amount field is required and positive.
        """
        serializer_data = deepcopy(self.serializer_data)
        del serializer_data['amount']
        serializer = TransactionSerializer(data=serializer_data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('amount', serializer.errors)

        serializer_data['amount'] = "-1.00"
        serializer = TransactionSerializer(data=serializer_data)
        self.assertFalse(serializer.is_valid())
        self.assertIn('amount', serializer.errors)

        serializer_data['amount'] = "1.00"
        serializer = TransactionSerializer(data=serializer_data)
        self.assertTrue(serializer.is_valid(), msg=serializer.errors)
