from accounts.serializers import UserSerializer
from accounts.models import BaseUser, User
from copy import deepcopy
from django.test import TestCase


class TestUserSerializer(TestCase):
    """
    Tests the UserSerializer.
    """
    @classmethod
    def setUpClass(cls):
        """
        Creates a dummy user to test the serializer.
        """
        super().setUpClass()
        cls.base_user_attributes = {
            "username": "asalmi",
            "first_name": "Amine",
            "last_name": "Salmi",
        }
        cls.user_attributes = {
            "fund": "150.35"
        }
        base_user = BaseUser.objects.create(**cls.base_user_attributes)
        User.objects.filter(user=base_user).update(**cls.user_attributes)
        cls.user = User.objects.get(user=base_user)
        cls.user_serializer = UserSerializer(instance=cls.user)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes BaseUsers, Users and Tokens created in setUpClass.
        """
        super().tearDownClass()
        BaseUser.objects.all().delete()

    def test_contains_expected_content(self):
        data = self.user_serializer.data
        expected_fields = ['username', 'first_name', 'last_name', 'fund']
        self.assertCountEqual(data.keys(), expected_fields,
                              msg=f"Discrepancy in fields found in serializer, expected {expected_fields}, found {list(data.keys())}")

        expected_content = deepcopy(self.base_user_attributes)
        expected_content.update(self.user_attributes)
        self.assertDictEqual(dict(data), expected_content)
