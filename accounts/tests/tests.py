from accounts.models import BaseUser, Token, User
from django.test import TestCase


class TestSignals(TestCase):
    """
    Signals ensure that when a User is created, a Token and a Profile (accounts.User) is automatically created.
    """

    @classmethod
    def tearDownClass(cls):
        """
        Destroys created users.
        """
        super().tearDownClass()
        BaseUser.objects.all().delete()

    def test_signals(self):
        """
        Creates BaseUsers and check it led to the creation of Tokens and Users.
        """
        self.assertEqual(BaseUser.objects.count(), 0)
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(Token.objects.count(), 0)
        for i in range(5):
            BaseUser.objects.create(username=f"test-{i}")
            self.assertTrue(User.objects.filter(user__username=f"test-{i}").exists())
            self.assertTrue(Token.objects.filter(user__username=f"test-{i}").exists())
