from accounts.models import BaseUser, User
from accounts.serializers import UserSerializer
from accounts.views import UserFund
from django.test import TestCase
from rest_framework.test import force_authenticate, APIRequestFactory

# Using the standard RequestFactory API to create an HTTP request
factory = APIRequestFactory()


class TestUserViews(TestCase):
    """
    Tests the view that retrieve's a user's details and fund.
    """

    @classmethod
    def setUpClass(cls):
        """
        Creates 5 dummy users with funds.
        """
        super().setUpClass()
        cls.expected_responses = []
        for i in range(5):
            BaseUser.objects.create(username=f"test-{i}")
            u = User.objects.get(user__username=f"test-{i}")
            u.fund = f"{i * 50}.00"
            u.save()
            cls.expected_responses.append(u)

    @classmethod
    def tearDownClass(cls):
        """
        Deletes users created
        """
        super().tearDownClass()
        BaseUser.objects.all().delete()

    def test_get_fund(self):
        """
        Tests for each user created in setUp that we can use the UserFund view to retrieve their information.
        """
        view = UserFund.as_view()
        request = factory.get('api/v1/funds')
        for user in self.expected_responses:
            force_authenticate(request, user=user.user)
            response = view(request)
            serializer = UserSerializer(user)
            self.assertEqual(response.data, serializer.data)

    def test_non_authenticated(self):
        """
        Checks that the user must be authenticated for the request.
        """
        view = UserFund.as_view()
        request = factory.get('api/v1/funds')
        for user in self.expected_responses:
            response = view(request)
            self.assertIn('detail', response.data)
            self.assertIn('Authentication', response.data['detail'])
