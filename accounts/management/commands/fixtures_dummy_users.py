from accounts.models import BaseUser, User
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Creates dummy users to test the application.
    Not something to do in production!
    """
    def handle(self, **options):
        for i in range(5):
            base_user, _ = BaseUser.objects.get_or_create(username=f"dummy-{i}",
                                                          defaults={"first_name": f"Dummy {i}", "last_name": "Smith"})
            self.stdout.write(f"Created {base_user.username}")
            base_user.set_password("DummyPassword123")
            base_user.save()
            user = User.objects.get(user__username=base_user.username)
            user.fund = f"{(i + 1) * 50}.00"
            user.save()
