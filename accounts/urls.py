from accounts import views
from django.urls import path
from django.views.generic.base import TemplateView


urlpatterns = [
    path('profile/', TemplateView.as_view(template_name='registration/home.html'), name='home'),
    path('api/v1/funds', views.UserFund.as_view(), name='user_funds'),
]
