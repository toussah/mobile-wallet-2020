from accounts.serializers import UserSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class UserFund(APIView):
    """
    Display user's fund as well as other personal details such as the name.
    The user must be authenticated or being using Token Authentication.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = UserSerializer(request.user.user)
        return Response(user.data)
