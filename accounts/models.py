from django.contrib.auth.models import User as BaseUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class User(models.Model):
    user = models.OneToOneField(BaseUser, on_delete=models.CASCADE, related_name="user")
    fund = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=BaseUser)
def post_create_user(sender, instance=None, created=False, **kwargs):
    """
    Each new user should have an associated account and token
    """
    if created:
        User.objects.create(user=instance)
        Token.objects.create(user=instance)
