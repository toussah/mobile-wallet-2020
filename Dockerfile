FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
WORKDIR /src
COPY requirements.txt /src/
COPY requirements-dev.txt /src/
RUN pip install --upgrade pip && \
    pip install -U flake8 && \
    pip install --no-cache-dir -r requirements.txt -r requirements-dev.txt
COPY . /src/
RUN flake8 --ignore=E501
