if [ $# -ne 1 ]
then
    echo "Usage: ./obtain_token.sh <username>"
    exit 1
fi

echo `curl -XPOST http://0.0.0.0:8000/api-token-auth/ -d '{"username": "'"$1"'", "password": "DummyPassword123"}' -H 'Content-Type: application/json' | jq -r '.token'`
