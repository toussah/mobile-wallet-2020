#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Usage: ./fund.sh <username>"
    exit 1
fi

token=`./obtain_token.sh $1`
if [ "$token" = "null" ]
then
    echo "Invalid username: $1"
    exit 1
fi

curl_opts=(
    -H "'Authorization: Token ${token}'"
)
url=http://0.0.0.0:8000/accounts/api/v1/funds
echo curl "${curl_opts[@]}" "$url"
