#!/bin/bash

if [ $# -lt 3 ]
then
    echo "Usage: ./fund.sh <sender_username> <receiver_username> <transfer_amount> [<reason>]"
    exit 1
fi

token=`./obtain_token.sh $1`
if [ "$token" = "null" ]
then
    echo "Invalid username: $1"
    exit 1
fi


if [ $# -ne 4 ]
then
    reason=''
else
    reason=$4
fi

curl_opts=(
    -H "'Authorization: Token ${token}'"
    -H "'Content-Type: application/json'"
    -d "'{\"amount\": \"${3}\", \"reason\": \"${reason}\"}'"
)
url=http://0.0.0.0:8000/transaction/api/v1/transfer/$2
echo curl "${curl_opts[@]}" "$url"
