import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mobilewallet-pkg-AMINE-SALMI", # Replace with your own username
    version="0.0.1",
    author="Amine Salmi",
    author_email="amine.f.salmi@gmail.com",
    description="Minimalist fund transfer Django API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/toussah/mobile-wallet-2020",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
