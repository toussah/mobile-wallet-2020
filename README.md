# Mobile Wallet 2020

## Docker:

```
sudo docker-compose run web python manage.py migrate
docker-compose build
docker-compose up -d
docker-compose exec web python manage.py fixtures_dummy_users
```

## Shell scripts:

5 dummy users are created with the same password through `fixtures_dummy_users.py`

They have the same passwords and an amount of fund attributes.

Their names are dummy-1, dummy-2, ..., dummy-5


The API works with Token authentication.

Some shell scripts have been added to generate the curl requests for the different API endpoints.

obtain_token.sh makes a request that retrieves the user's token, it is used by the other shell scripts.

fund.sh, transactions.sh, and transfer.sh don't make any call, they only echo the curl

command that you can then run.

e.g.

```
cd scripts/
./obtain_token.sh dummy-1
./fund.sh dummy-1
./transactions.sh dummy-1
./transfer.sh dummy-1 dummy-2 <amount> [<reason>]
```
